﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {

	// separate speed and direction so we can 
	// tune the speed without changing the code
	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();    
	}

	void FixedUpdate () {
		rigidbody.velocity = speed * direction;
	}

	void OnCollisionEnter(Collision collision) {
		// Destroy the bullet
		Destroy(gameObject);
	}

}
