﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;

	void Update () {
		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown ("Fire1")) {

			BulletMove bullet = Instantiate (bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay (Input.mousePosition);
			bullet.direction = ray.direction;
		}
	}
}
